<?php
/**
 * SkillsAndMore Blocks with Metabox.io
 *
 * @package           SkillsAndMoreMBBlocks
 * @author            Andrea Barghigiani
 * @copyright         2019 SkillsAndMore
 * @license           GPL-2.0-or-later
 *
 * @wordpress-plugin
 * Plugin Name: SkillsAndMore Blocks with Metabox.io
 * Plugin URI: https://skillsandmore.org
 * Description: Questo plugin aggiunge nuovi blocchi creati con la libreria Metabox.io.
 * Author: Andrea Barghigiani
 * Version: 0.0.1
 * Text Domain: sam-mb-blocks
 */

// If this file is called directly, abort.
if ( ! defined( 'ABSPATH' ) ) {
	die;
}

// Carico i pacchetti installati da Composer.
require plugin_dir_path( __FILE__ ) . 'vendor/autoload.php';

add_filter( 'block_categories', 'sam_block_categories' );
/**
 * Add a block category for "Get With Gutenberg" if it doesn't exist already.
 *
 * @param array $categories Array of block categories.
 *
 * @return array
 */
function sam_block_categories( $categories ) {
	$category_slugs = wp_list_pluck( $categories, 'slug' );
	return in_array( 'sam', $category_slugs, true ) ? $categories : array_merge(
		$categories,
		array(
			array(
				'slug'  => 'sam',
				'title' => __( 'SkillsAndMore', 'sam-mb-blocks' ),
				'icon'  => null,
			),
		)
	);
}

add_filter( 'rwmb_meta_boxes', 'sam_mb_boxes' );
/**
 * Funzione generale per dichiarare tutti i blocchi creati con Metabox.io
 *
 * @param array $meta_boxes L'array che contiene tutti i blocchi e meta box dichiarati.
 *
 * @return array $meta_boxes
 */
function sam_mb_boxes( $meta_boxes ) {
	$prefix = 'sam-b';
	// Dichiaro il mio primo blocco.

	$meta_boxes[] = [
		'title' => __( 'SAM Pricing Table', 'sam-mb-blocks' ),
		'id' => $prefix . '-pricing-table',
		'description' => __( 'La nuova tabella dei prezzi', 'sam-mb-blocks' ),
		'type' => 'block',
		'icon' => '<svg xmlns="http://www.w3.org/2000/svg" height="24" viewBox="0 0 24 24" width="24"><path d="M11.8 10.9c-2.27-.59-3-1.2-3-2.15 0-1.09 1.01-1.85 2.7-1.85 1.78 0 2.44.85 2.5 2.1h2.21c-.07-1.72-1.12-3.3-3.21-3.81V3h-3v2.16c-1.94.42-3.5 1.68-3.5 3.61 0 2.31 1.91 3.46 4.7 4.13 2.5.6 3 1.48 3 2.41 0 .69-.49 1.79-2.7 1.79-2.06 0-2.87-.92-2.98-2.1h-2.2c.12 2.19 1.76 3.42 3.68 3.83V21h3v-2.15c1.95-.37 3.5-1.5 3.5-3.55 0-2.84-2.43-3.81-4.7-4.4z"/><path d="M0 0h24v24H0z" fill="none"/></svg>',
		'category' => 'sam',
		'render_template' => plugin_dir_path( __FILE__ ) . 'blocks/price-table/template.php',
		'enqueue_style' => plugin_dir_url( __FILE__ ) . 'blocks/price-table/style.css',
		'fields' => [
			[
				'type' => 'text',
				'id' => 'title',
				'name' => 'Titolo',
			],
			[
				'type' => 'checkbox',
				'id' => 'highlighted',
				'name' => 'In evidenza?',
			],
			[
				'type' => 'color',
				'id' => 'highlight_color',
				'name' => 'Colore in evidenza',
			],
			[
				'type' => 'group',
				'id' => 'value_group',
				'name' => 'Elenco vantaggi',
				'clone'       => true,
				'sort_clone'  => true,
				'collapsible' => true,
                'group_title' => array( 'field' => 'value_text' ), // ID of the subfield
				'fields' => [
					[
						'type' => 'text',
						'id' => 'value_text',
						'name' => 'Vantaggio',
						'columns' => 8,
					],
					[
						'type' => 'checkbox',
						'id' => 'value_barrato',
						'name' => 'Barrato?',
						'columns' => 4,
					]

				]
			],
			[
				'type' => 'text',
				'id' => 'btn_text',
				'name' => 'Testo Pulsante',
			],
			[
				'type' => 'text',
				'id' => 'btn_url',
				'name' => 'URL Pulsante',
			],
		],
	];

	return $meta_boxes;
}
